package Polymorphism;

public class Hewan {
    public void suaraHewan(){
        System.out.println("suara hewan");
    }
}

class Burung extends Hewan{
    public void suaraHewan(){
        System.out.println("suara burung = cuit cuit");
    }
}

class Kucing extends Hewan{
    public void suaraHewan(){
        System.out.println("suara kucing = meong meong");
    }

}

class main {
    public static void main(String[] args) {
        Hewan hewan = new Hewan();
        Hewan burung = new Burung();
        Hewan kucing = new Kucing();
        hewan.suaraHewan();
        burung.suaraHewan();
        kucing.suaraHewan();
    }
}