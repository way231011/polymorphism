package Polymorphism;

public class MainManusia {
    public static void main(String[] args) {
        Manusia manusia = new Manusia();
        Manusia modelManusia = new ModelManusia();
        Manusia panggilManusia = new PanggilManusia();
        manusia.manusiaKelakuan();
        modelManusia.manusiaKelakuan();
        panggilManusia.manusiaKelakuan();
    }
}
